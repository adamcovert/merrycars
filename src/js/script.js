$(document).ready(function() {
  svg4everybody();

  $('.s-hamburger').on('click', function () {
    if ( !$('.s-main-nav').hasClass('s-main-nav--is-active') ) {
      $('.s-main-nav').addClass('s-main-nav--is-active');
      $('.s-hamburger').addClass('s-hamburger--is-active');
      $('body').addClass('prevent-scroll');
    } else {
      $('.s-main-nav').removeClass('s-main-nav--is-active');
      $('.s-hamburger').removeClass('s-hamburger--is-active');
      $('body').removeClass('prevent-scroll');
    }
  });

  $('.s-testimonials__slider').owlCarousel({
    margin: 30,
    dots: false,
    responsive: {
      0: {
        items: 1,
        nav: true,
        navText: ['<svg class="s-main-slider__navs-btn" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg"><g transform="translate(1 1)" stroke="#F0A631" fill="none" fill-rule="evenodd"><circle cx="19" cy="19" r="19"/><path stroke-width="2" stroke-linecap="round" d="M21 13l-5 6 5 6"/></g></svg>', '<svg class="s-main-slider__navs-btn" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg"><g transform="translate(1 1)" stroke="#F0A631" fill="none" fill-rule="evenodd"><circle cx="19" cy="19" r="19"/><path stroke-width="2" stroke-linecap="round" d="M21 13l-5 6 5 6"/></g></svg>']
      },
      992: {
        items: 3
      }
    }
  });

  $('.s-special-offers__slider').owlCarousel({
    margin: 30,
    dots: false,
    responsive: {
      0: {
        items: 1,
        nav: true,
        navText: ['<svg class="s-main-slider__navs-btn" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg"><g transform="translate(1 1)" stroke="#F0A631" fill="none" fill-rule="evenodd"><circle cx="19" cy="19" r="19"/><path stroke-width="2" stroke-linecap="round" d="M21 13l-5 6 5 6"/></g></svg>', '<svg class="s-main-slider__navs-btn" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg"><g transform="translate(1 1)" stroke="#F0A631" fill="none" fill-rule="evenodd"><circle cx="19" cy="19" r="19"/><path stroke-width="2" stroke-linecap="round" d="M21 13l-5 6 5 6"/></g></svg>']
      },
      992: {
        items: 2,
        stagePadding: 10,
      }
    }
  });

  $('.s-car-park__slider').owlCarousel({
    margin: 30,
    dots: false,
    responsive: {
      0: {
        items: 2,
        nav: true,
        navText: ['<svg class="s-main-slider__navs-btn" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg"><g transform="translate(1 1)" stroke="#F0A631" fill="none" fill-rule="evenodd"><circle cx="19" cy="19" r="19"/><path stroke-width="2" stroke-linecap="round" d="M21 13l-5 6 5 6"/></g></svg>', '<svg class="s-main-slider__navs-btn" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg"><g transform="translate(1 1)" stroke="#F0A631" fill="none" fill-rule="evenodd"><circle cx="19" cy="19" r="19"/><path stroke-width="2" stroke-linecap="round" d="M21 13l-5 6 5 6"/></g></svg>']
      },
      448: {
        items: 3
      },
      992: {
        items: 6
      }
    }
  });

  $('.s-gallery__thumbs').owlCarousel({
    nav: false,
    dots: false,
    loop: true,
    center: true,
    margin: 5,
    responsive: {
      0: {
        items: 3,
      },
      1200: {
        items: 5
      }
    }
   });

  $('.s-gallery__thumbs').on('click', 'img', function () {
    var carousel = $('.s-gallery__thumbs').data('owl.carousel');
    carousel.to(carousel.relative($(this).parent().parent().index()), false, true);
  });

  $('.s-gallery__thumbs').on('changed.owl.carousel', function () {
    setTimeout(function (){
      var img = $('.s-gallery__thumbs').find('.owl-item.center div img').attr('src');
      $('.s-gallery__cover-image-full').attr('src', img);
    }, 100);
  });



  $('[type="tel"]').mask('+7 (000) 000-00-00');



  $('select').each(function(){
    var $this = $(this),
        numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });
  });

  $("[data-fancybox]").fancybox({
    // Options will go here
  });
});